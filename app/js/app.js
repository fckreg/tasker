+(function() {
angular
  .module("TaskerApp", ["ui.router"])
  .config([
    "$stateProvider",
    "$locationProvider",
    function($stateProvider, $locationProvider) {
      $stateProvider
        .state("login", {
          url: "/",
          templateUrl: "login.html",
          controller: "LoginController"
        })
        .state("tasks", {
          url: "/tasks",
          templateUrl: "tasks.html",
          controller: "TaskListController"
        })
        .state("task", {
          url: "/task/{taskId}",
          templateUrl: "task.html",
          controller: "TaskController"
        });
      $locationProvider.html5Mode({
        enabled: true,
        requireBase: false
      });
    }
  ])
  .run([
    "$state",
    "$transitions",
    "SessionService",
    function($state, $transitions, SessionService) {
      $transitions.onEnter(
        {
          to: function(state) {
            return state.name.indexOf("task") === 0;
          },
          from: "*"
        },
        SessionService.checkAccess
      );
      $transitions.onEnter({ to: "login", from: "*" }, SessionService.redirectFromLogin);
    }
  ]);
})();