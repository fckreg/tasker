+(function() {
  angular.module("TaskerApp").factory("dataService", dataService);

  function dataService() {
    var tasks = {
      admin: [
        {
          id: "001",
          title: "Установка и настройка сборщика проекта",
          detail: `Установка и настройка сборщика проекта`,
          author: "Иванов И.И.",
          date: "1494164908629",
          status: "готово",
          priority: "средний",
          planTime: "60",
          factTime: "45",
          comments: [
            {
              date: 423423423232,
              text: `Установка и настройка сборщика проекта.`,
              author: "Admin"
            }
          ]
        },
        {
          id: "002",
          title: "Вход в приложение, авторизация.",
          detail: `Приложение должно содержать два уровня доступа. Неавторизованному пользователю должна быть доступна только страница ввода данных (логин, пароль) 
                            После аутентификации пользователь получает доступ к своему списку задач.`,
          author: "Иванов И.И.",
          date: "1494164908629",
          status: "в работе",
          priority: "средний",
          planTime: "60",
          factTime: "45",
          comments: [
            {
              date: 423423423232,
              text: "Приложение должно содержать два уровня доступа. Неавторизованному пользователю должна быть доступна только страница ввода данных (логин, пароль) После аутентификации пользователь получает доступ к своему списку задач.",
              author: "Admin"
            }
          ]
        },
        {
          id: "003",
          title: "Список задач.",
          detail: `Список задач пользователя, представленный в табличном виде, с возможностью сортировки
                            списка, фильтрации (к примеру - по статусам) и смены представления (подробный/краткий вид/scrum доска)`,
          author: "Петров П.П.",
          date: "1494465908629",
          status: "новая",
          priority: "средний",
          planTime: "60",
          factTime: "45",
          comments: []
        },
        {
          id: "004",
          title: "Страница задачи",
          detail: `Страница с подробной информацией о задаче - название, описание, дата, приоритет,
планируемое и затраченное время, статус выполнения.`,
          author: "Сидоров С.С.",
          date: "1497860908629",
          status: "новая",
          priority: "средний",
          planTime: "60",
          factTime: "45",
          comments: [
            {
              date: 423423423232,
              text: `Страница с подробной информацией о задаче - название, описание, дата, приоритет,
планируемое и затраченное время, статус выполнения.`,
              author: "Admin"
            },
            {
              date: 423423423232,
              text: `Страница с подробной информацией о задаче - название, описание, дата, приоритет,
планируемое и затраченное время, статус выполнения.`,
              author: "Admin"
            }
          ]
        }
      ],
      user : [
        {
          id: "005",
          title: "Добавление второго пользователя",
          detail: `Необходимо реализовать возможность входа под несколькими пользователями`,
          author: "Александров А.А.",
          date: "1424164908629",
          status: "готово",
          priority: "высокий",
          planTime: "60",
          factTime: "45",
          comments: [
            {
              date: 423423423232,
              text: `Добавление второго пользователя`,
              author: "Admin"
            }
          ]
        }
      ]
    };
    function getUserTasks(userName) {
      return tasks[userName];
    }
    function getTaskById(userName, taskId) {
      var index = getTaskIndex(userName, taskId);
      return tasks[userName][index];
    }

    function getTaskIndex(userName, taskId) {
      for (var i = 0; i < tasks[userName].length; i++) {
        if (tasks[userName][i].id == taskId) {
          break;
        }
      }
      return i;
    }

    function addComment(userName, taskId, comment) {
      if (comment.length > 0) {
        var index = getTaskIndex(userName, taskId);
        var userName = localStorage.userName;
        console.log(index);
        console.log(tasks[userName][index]);
        tasks[userName][index].comments.push({ date: Date.now(), text: comment, author: userName });
      }
    }

    return {
      getUserTasks: getUserTasks,
      getTaskById: getTaskById,
      addComment: addComment
    };
  }
})();
