+(function() {
  angular.module("TaskerApp").service("SessionService", SessionService);

  SessionService.$inject = ["$state"];

  function SessionService($state) {
      "use strict";
      this.isLoggedIn = isLoggedIn;

      this.checkAccess = function(event, toState, toParams, fromState, fromParams) {
        if (!isLoggedIn()) {
          $state.go("login");
          return false;
        }

        return true;
      };

      this.redirectFromLogin = function() {
        if (isLoggedIn()) {
          $state.go("tasks");
        }
      };

      this.checkLogin = function(name, pass) {
        if ((name == "admin" && pass == "admin") || (name == "user" && pass == "user")) {
          localStorage.accessToken = "sometoken";
          localStorage.userName = name;
          return true;
        }

        return false;
      };

      function isLoggedIn() {
        return localStorage.accessToken == "sometoken";
      }
    }

})();
