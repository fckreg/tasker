+(function() {
  angular.module("TaskerApp").controller("LoginController", LoginController);

  LoginController.$inject = ["$scope", "$state", "SessionService"];

  function LoginController($scope, $state, SessionService) {
    
    // настройка контроллера

    $scope.pass = "";
    $scope.name = "";

    $scope.popupVisibility = false;
    $scope.popupClose = function() {
      $scope.popupVisibility = false;
    };

    $scope.isLoggedIn = function() {
      return SessionService.isLoggedIn();
    };
    $scope.login = function() {
      if (SessionService.checkLogin($scope.name, $scope.pass)) {
        $state.go("tasks");
      } else {
        $scope.popupVisibility = true;
      }
    };

    $scope.logout = function() {
      localStorage.accessToken = null;
      localStorage.userName = "Здесь будет имя";
      $state.go("login");
    };
  }
})();
