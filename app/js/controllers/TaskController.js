+(function() {
  angular.module("TaskerApp").controller("TaskController", TaskController);

  TaskController.$inject = ["$scope", "$stateParams", "$state", "dataService"];

  function TaskController($scope, $stateParams, $state, dataService) {
    
    // настройка контроллера

    var taskId = $stateParams.taskId;
    var userName = localStorage.userName;
    $scope.task = dataService.getTaskById(userName, taskId);

    if (!$scope.task) {
      return $state.go("tasks");
    }

    $scope.comment = "";
    $scope.addComment = function(comment) {
      dataService.addComment(userName, taskId, comment);
      $scope.comment = "";
    };
  }
})();
