+(function() {
  angular.module("TaskerApp").controller("TaskListController", TaskListController);

  TaskListController.$inject = ["$scope", "dataService"];

  function TaskListController($scope, dataService) {

    // настройка контроллера

    var userName = localStorage.userName;
    $scope.detailView = false;
    $scope.sortType = "title"; // значение сортировки по умолчанию
    $scope.sortReverse = true; // обратная сортировка
    $scope.searchTasks = ""; // значение поиска по умолчанию
    $scope.tasks = dataService.getUserTasks(userName);
  }
})();
