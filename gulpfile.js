var gulp = require("gulp"),
    livereload = require("gulp-livereload"),
    sass = require("gulp-sass"),
    concat = require("gulp-concat"),
    connect = require("connect"),
    serveStatic = require("serve-static"),
    angularTemplateCache = require("gulp-angular-templatecache"),
    wiredep = require('wiredep').stream,
    useref = require('gulp-useref'),
    gulpif = require('gulp-if'),
    uglify = require('gulp-uglify'),
    minifyCss = require('gulp-clean-css'),
    clean = require('gulp-clean');


gulp.task("templates", function() {
  gulp
    .src("./app/html/templates/**/*.html")
    .pipe(
      angularTemplateCache("templates.js", {
        module: "TaskerApp"
      })
    )
    .pipe(gulp.dest("./app/js/"));
});

gulp.task("html", function() {
  gulp
  .src("./app/index.html")
  .pipe(gulp.dest("./app/"))
  .pipe(livereload());
});

gulp.task("wiredep", function() {
  gulp
  .src("./app/index.html")
  .pipe(wiredep({
      directory: "./app/bower_components",
      exclude: ['/jquery/'],
    }))
  .pipe(gulp.dest("./app"));
});

gulp.task("sass", function() {
  gulp
    .src("./app/scss/**/*.scss")
    .pipe(sass())
    .pipe(concat("main.css"))
    .pipe(gulp.dest("./app/css/"))
    .pipe(livereload());
});

gulp.task("js", function() {
  gulp
    .src("app/js/**/*.js")
    .pipe(concat("all.js"))
    .pipe(gulp.dest("./app/js/"))
    .pipe(livereload());
});

gulp.task("create dist", function(){
  return gulp.src('app/index.html')
    .pipe(useref())
    .pipe(gulpif('*.css', minifyCss()))
    .pipe(gulp.dest('dist'));
});

gulp.task("clean", function () {
    return gulp.src('dist', {read: false})
        .pipe(clean());
});

gulp.task("start", ["html", "templates", "sass", "js", "wiredep"]);
gulp.task("build", ["clean", "create dist"]);

/*
 * Создадим веб-сервер, чтобы работать с проектом через браузер
 */
gulp.task("server", function() {
  connect()
  .use(require("connect-livereload")())
  .use(serveStatic(__dirname + "/app"))
  .listen("3333");

  console.log("Сервер работает по адресу http://localhost:3333");
});

/*
 * Создадим задачу, смотрящую за изменениями
 */
gulp.task("watch", function() {
  livereload.listen();
  gulp.watch(["bower.json"], ["wiredep"]);
  gulp.watch(["app/js/**/*.js"], ["js"]);
  gulp.watch(["app/scss/**/*.scss"], ["sass"]);
  gulp.watch(["app/index.html"], ["html"]);
  gulp.watch(["app/html/templates/**/*.html"], ["templates"]);
  gulp.start("server");
});

gulp.task("default", ["start", "watch"]);
